console.log('Hello World');


				// 3. Obeject Literals
let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']},
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Balbasaur'],
	talk: function(){
		console.log(`${trainer.pokemon[0]}! I choose you!`)
	}

}

console.log(trainer);
console.log(trainer.name);
console.log(trainer.pokemon);
trainer.talk();


				// 8. Constructor
function pokemon(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

// 10.Create tackle
	this.tackle = function(target){
		/*if(target <= 0){
			// console.log(`${target.name} is fainted`)
		}*/	
			console.log(`${this.name} tackled ${target.name}`);
			console.log(`${target.name} health is now reduced to ${target.health - this.attack}`);
	} 
	this.faint = function(){
		console.log(`${this.name} fainted`)
	}
}

// 9.create several pokemon
	let pikachu = new pokemon('Pikachu', 12, );
	let geodude = new pokemon('Geodude', 8, );
	let mewtwo = new pokemon('Mewtwo', 100, );
	
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);

mewtwo.tackle(geodude);

geodude.faint();


// ONGOING ACTIVITY..... :) 
