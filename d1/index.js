console.log('hello world')


// OBJECTS	

// creating objetcts using object initializers/literal notation
//  {} = Object literals

				// sample for OBJECT LITERALS

let cellphone = {
	name: 'Nokia 3310',
	manufactureDate: 1999
}

console.log(cellphone);
console.log(typeof cellphone);


// access using dot notation
console.log(cellphone.name);


let cellphone2 = {
	name: 'RealMe super zoom',
	manufactureDate: 2019
}

let cellphone3 = {
	name: 'Samsung A7',
	manufactureDate: 2017
}

// creating objects using a constructor function
// creates a reusable function to create several objetcs that have the same data structure.
/*
	syntax:
			function objectName(keyA, keyB){
				this.keyA = keyA;
				this.keyB = keyB;
			}
*/


					// sample for CONSTRUCTOR FUNCTION

function Laptop(name, os, price){
	this.name = name;
	this.os = os;
	this.price = price;
}

// this is a unique INSTANCE of the laptop object.
// the 'NEW' operator create an instance of an object.
// object literals
		/*
			let object = {
				key: value
			}
		*/

// Instances

		/*
			let object = new object

		*/

let laptop1 = new Laptop('Lenovo', 'Windows 10', 30000);
console.log(laptop1);


// another unique instance of the laptop object
let myLaptop = new Laptop('Macbook Pro', 'Catalina', 50000);
console.log(myLaptop);



//ways of creating empty objects
let computer = {} //sample1
// or
let myComputer = new Object() //sample2



// Ways of Accessing Object Properties
// 1. using the dot notation
console.log(myLaptop.name);

// 2. using the square bracket notation
console.log(myLaptop['name']);

// Array of Objects
let array = [laptop1, myLaptop];
console.log(array);

console.log(array[0].name); //right way
console.log(array[0]['name']);


//Initializing/Adding/deleting/Reassigning Object Properties


// sample

let car = {};
console.log(car);

// initializing/adding obejct properties using dot notation
car.name = 'Honda Civic';
console.log('Result from adding properties');
console.log(car);

//adding object properties using bracket notation
car['manufacture date'] = 2019;
console.log(car);
console.log(car['manufacture date']);

// deleting object properties

delete car['manufacture date'];
console.log('Result from deleting properties');
console.log(car);


// reassigning object properties
car.name = 'Dodge Charger RT';
console.log('Reassigning properties')
console.log(car);


// Object Methods
// a method is a function which is a property of an object

let person = {
	name: 'John',
	talk: function(){
		console.log(`Hello my name is ${this.name}`)
	}
}


person.talk();
console.log(person);


person.walk = function(){
	console.log(this.name + ' walked 25 steps forward')
}

person.walk();

let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address:{
		city: 'Austin',
		country: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function(){
		console.log(`Hello my name is ${this.firstName} ${this.lastName}. I live at ${this.address.city}, ${this.address.country}`);
	}
}
friend.introduce();



// scenario

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log('This pokemon tackled targetPokemon');
		console.log("targetPokemon's health is now reduce to _targetPokemonhealth_")
	}
	faint: function(){
		console.log('pokemon fanted')
	}
}


// creating an object constructor instead of object literals

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;

	//methods
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`);
		console.log("targetPokemon's health is now reduced.")
	}
	this.faint = function(){
		console.log(`${this.name} fainted`)
	}
}

// create new instances
let pikachu = new Pokemon('Pikachu', 16);
let ratata = new Pokemon('Ratata', 8);

pikachu.tackle(ratata);
ratata.tackle(pikachu);

ratata.faint();





















































































































































